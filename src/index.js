import React from 'react'
import ReactPDF from '@react-pdf/renderer'
import fetch from 'node-fetch'
import MainPdf from './MainPdf'

const url = 'https://fotostory.oklaspec.com/yourbook/ajax/editor/orderposts?id=1&scrhp=5f8ceb9'

const argv = process.argv.slice(2)
const file = argv[0]

console.log(file);

const render = (data, file) => {
  ReactPDF.render(<MainPdf data={data} />, file)
}

// const from_console = () => {

//   let input = ''

//   process.stdin.resume()
//   process.stdin.setEncoding('utf8')

//   process.stdin.on('data', function (chunk) {
//     input += chunk
//   });

//   process.stdin.on('end', function () {
//     var parsedData = JSON.parse(input)
//     var data = JSON.stringify(parsedData, null, '    ')
//     render(data, file)
//   })
// }

const from_url = url => {
  fetch(url)
    .then(data => data.json())
    .then(data => render(data, file))
}

from_url(url)
