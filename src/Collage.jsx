import React from 'react';
import { View, Image, StyleSheet } from '@react-pdf/renderer';

const styles = StyleSheet.create({
  image: {
    width: '100%',
    objectFit: 'cover',
  },
  item: {
    margin: 1,
    width: '32%',
    flexGrow: 1,
  },
  collage: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flexBasis: '75%',
  }
})

const Collage = props => {
  return (
    <View style={styles.collage}>
      {props.photos.map(photo => {
        return (
          <div style={styles.item} key={Math.random()}>
            <Image style={styles.image} src={photo} />
          </div>
        )
      })}
    </View>
  )
}

export default Collage