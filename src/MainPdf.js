import React from 'react'
import { Document, Page, Text, View, Image, StyleSheet } from '@react-pdf/renderer'
import Collage from './Collage'

const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4',
    justifyContent: 'center',
  },
  section: {
    flexDirection: 'row',
  }
})

const photos = [
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg',
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg',
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg',
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg',
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg',
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg',
  'https://klike.net/uploads/posts/2019-07/1561958839_1.jpg'
]

function MainPdf(props) {
  console.log(props.data);
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <Collage photos={photos} />
      </Page>
    </Document>
  )
}

export default MainPdf
